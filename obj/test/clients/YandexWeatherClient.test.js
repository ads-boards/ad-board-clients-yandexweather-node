"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const src_1 = require("../../src");
let assert = require('chai').assert;
let async = require('async');
suite('YandexWeatherLogClient', () => {
    let _client;
    setup((done) => {
        let apiKey = process.env['YANDEX_API_KEY'] || "951d4791-0ba4-450e-9352-96997f7dd49c";
        _client = new src_1.YandexWeatherClientV1();
        let config = pip_services3_commons_node_1.ConfigParams.fromTuples('credential.access_key', apiKey, 'options.update_interval', 1000, // 1 sec
        'options.lat', '55.75396', 'options.lon', '37.620393', "options.method", "forecast" // for test key
        //"options.method", "informers" // for other key
        );
        _client.configure(config);
        _client.open(null, (err) => {
            done(err);
        });
    });
    teardown((done) => {
        _client.close(null, done);
    });
    test('Test autoupdate weather cache', (done) => {
        async.series([
            (callback) => {
                setTimeout(() => {
                    callback();
                }, 2000);
            },
            (callback) => {
                _client.getWeatherFromCache("123", (err, cache) => {
                    assert.isNull(err);
                    assert.isNotNull(cache);
                    assert.isObject(cache);
                    assert.isNotNull(cache.last_update);
                    assert.isObject(cache.weather);
                    callback();
                });
            }
        ], (err) => {
            done();
        });
    });
});
//# sourceMappingURL=YandexWeatherClient.test.js.map