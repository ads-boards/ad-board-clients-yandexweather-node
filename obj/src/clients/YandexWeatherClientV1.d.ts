import { ConfigParams } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { RestClient } from 'pip-services3-rpc-node';
import { WeatherCacheV1 } from './WeatherCacheV1';
export declare class YandexWeatherClientV1 extends RestClient {
    private _lat;
    private _lon;
    private _lang;
    private _method;
    private _timer;
    private _cache;
    private _correlationId;
    private _interval;
    private _defaultConfig;
    private _credentialResolver;
    constructor(config?: ConfigParams);
    configure(config: ConfigParams): void;
    setReferences(refs: IReferences): void;
    open(correlationId: string, callback: (err: any) => void): void;
    close(correlationId: string, callback: (err: any) => any): void;
    getWeather(correlationId: string, callback: (err: any, weather: any) => void): void;
    getWeatherFromCache(correlationId: string, callback: (err: any, weather: WeatherCacheV1) => void): void;
    private _updateCache;
}
