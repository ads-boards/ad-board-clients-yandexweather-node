"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YandexWeatherClientV1 = void 0;
/** @module clients */
const _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
const WeatherCacheV1_1 = require("./WeatherCacheV1");
class YandexWeatherClientV1 extends pip_services3_rpc_node_1.RestClient {
    constructor(config) {
        super();
        this._lat = '55.75396'; // Moscow
        this._lon = '37.620393';
        this._lang = "ru_RU";
        this._method = 'informers';
        this._timer = new pip_services3_commons_node_1.FixedRateTimer();
        this._correlationId = "ads.weather";
        this._interval = 30 * 60 * 1000; // 30 min;
        this._defaultConfig = pip_services3_commons_node_1.ConfigParams.fromTuples("connection.protocol", "https", "connection.host", "api.weather.yandex.ru", "connection.port", 443, "credential.internal_network", "true");
        this._credentialResolver = new pip_services3_components_node_1.CredentialResolver();
        if (config)
            this.configure(config);
        this._baseRoute = "v2";
    }
    configure(config) {
        config = this._defaultConfig.override(config);
        super.configure(config);
        this._credentialResolver.configure(config);
        this._interval = config.getAsIntegerWithDefault("options.update_interval", this._interval);
        this._lon = config.getAsStringWithDefault("options.lon", this._lon);
        this._lat = config.getAsStringWithDefault("options.lat", this._lat);
        this._lang = config.getAsStringWithDefault("options.lang", this._lang);
        this._method = this._lang = config.getAsStringWithDefault("options.method", this._method);
    }
    setReferences(refs) {
        super.setReferences(refs);
        this._credentialResolver.setReferences(refs);
    }
    open(correlationId, callback) {
        correlationId = correlationId || this._correlationId + " " + pip_services3_commons_node_1.IdGenerator.nextLong();
        this._credentialResolver.lookup(correlationId, (err, credential) => {
            if (err) {
                callback(err);
                return;
            }
            if (credential == null || credential.getAccessKey() == null) {
                err = new pip_services3_commons_node_2.ConfigException(correlationId, "NO_ACCESS_KEY", "Missing access key in credentials");
                callback(err);
                return;
            }
            this._headers = this._headers || {};
            this._headers['x-yandex-api-key'] = credential.getAccessKey();
            // start autoupdate weather into cache
            if (this._interval > 0) {
                this._timer.setDelay(this._interval);
                this._timer.setInterval(this._interval);
                this._timer.setTask({
                    notify: (correlationId, args) => {
                        this._updateCache(correlationId);
                    }
                });
                this._logger.info(correlationId, "Update weather cache processing is enable");
                this._timer.start();
            }
            super.open(correlationId, callback);
        });
    }
    close(correlationId, callback) {
        correlationId = correlationId || this._correlationId + " " + pip_services3_commons_node_1.IdGenerator.nextLong();
        this._timer.stop();
        this._logger.info(correlationId, "Update weather cache processing i disable");
        super.close(correlationId, callback);
    }
    getWeather(correlationId, callback) {
        let params = {
            lat: this._lat,
            lon: this._lon,
            lang: this._lang
        };
        let timing = this.instrument(correlationId, "ads.weather.get_weather");
        this.call("get", this._method, null, params, null, (err, result) => {
            timing.endTiming();
            this.instrumentError(correlationId, "ads.weather.get_weather", err, result, callback);
        });
    }
    getWeatherFromCache(correlationId, callback) {
        let timing = this.instrument(correlationId, "ads.weather.get_weather_from_cache");
        if (this._cache != null && this._cache.weather != null) {
            timing.endTiming();
            callback(null, this._cache);
        }
        else {
            this.getWeather(correlationId, (err, weather) => {
                if (err) {
                    timing.endTiming();
                    this.instrumentError(correlationId, "ads.weather.get_weather_from_cache", err, null, callback);
                    return;
                }
                if (weather == null) {
                    err = new pip_services3_commons_node_1.NotFoundException(correlationId, "NOT_FOUND", "Cache is empty and can't get weather from service");
                    timing.endTiming();
                    this.instrumentError(correlationId, "ads.weather.get_weather_from_cache", err, null, callback);
                    return;
                }
                this._cache = this._cache || new WeatherCacheV1_1.WeatherCacheV1();
                this._cache.last_update = new Date();
                this._cache.weather = weather;
                callback(null, this._cache);
            });
        }
    }
    _updateCache(correlationId) {
        let timing = this.instrument(correlationId, "ads.weather.update_weather_cache");
        this.getWeather(correlationId, (err, weather) => {
            if (err) {
                timing.endTiming();
                this.instrumentError(correlationId, "ads.weather.update_weather_cache", err, weather, null);
                return;
            }
            if (weather == null) {
                err = new pip_services3_commons_node_1.NotFoundException(correlationId, "NOT_FOUND", "The weather not available in service");
                timing.endTiming();
                this.instrumentError(correlationId, "ads.weather.update_weather_cache", err, weather, null);
                return;
            }
            this._cache = this._cache || new WeatherCacheV1_1.WeatherCacheV1();
            this._cache.last_update = new Date();
            this._cache.weather = weather;
        });
    }
}
exports.YandexWeatherClientV1 = YandexWeatherClientV1;
//# sourceMappingURL=YandexWeatherClientV1.js.map