"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultYandexWeatherFactory = void 0;
/** @module build */
const pip_services3_components_node_1 = require("pip-services3-components-node");
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const YandexWeatherClientV1_1 = require("../clients/YandexWeatherClientV1");
/**
 * Creates YandexWeather components by their descriptors.
 *
 * @see [[YandexWeatherLogger]]
 */
class DefaultYandexWeatherFactory extends pip_services3_components_node_1.Factory {
    /**
     * Create a new instance of the factory.
     */
    constructor() {
        super();
        this.registerAsType(DefaultYandexWeatherFactory.YandexWeatherClientDescriptor, YandexWeatherClientV1_1.YandexWeatherClientV1);
    }
}
exports.DefaultYandexWeatherFactory = DefaultYandexWeatherFactory;
DefaultYandexWeatherFactory.Descriptor = new pip_services3_commons_node_1.Descriptor("ad-board-yandexweather", "factory", "datadog", "default", "1.0");
DefaultYandexWeatherFactory.YandexWeatherClientDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-yandexweather", "client", "direct", "*", "1.0");
//# sourceMappingURL=DefaultYandexWeatherFactory.js.map