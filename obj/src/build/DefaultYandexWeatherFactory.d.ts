/** @module build */
import { Factory } from 'pip-services3-components-node';
import { Descriptor } from 'pip-services3-commons-node';
/**
 * Creates YandexWeather components by their descriptors.
 *
 * @see [[YandexWeatherLogger]]
 */
export declare class DefaultYandexWeatherFactory extends Factory {
    static readonly Descriptor: Descriptor;
    static readonly YandexWeatherClientDescriptor: Descriptor;
    /**
     * Create a new instance of the factory.
     */
    constructor();
}
