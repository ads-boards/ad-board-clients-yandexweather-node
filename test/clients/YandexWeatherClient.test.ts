import { ConfigParams } from 'pip-services3-commons-node';
import { YandexWeatherClientV1 } from '../../src';

let assert = require('chai').assert;
let async = require('async');

suite('YandexWeatherLogClient', () => {
    let _client: YandexWeatherClientV1;


    setup((done) => {
        let apiKey = process.env['YANDEX_API_KEY'] ;

        _client = new YandexWeatherClientV1();

        let config = ConfigParams.fromTuples(
            'credential.access_key', apiKey,
            'options.update_interval', 1000, // 1 sec
            'options.lat', '55.75396',
            'options.lon', '37.620393',
            "options.method", "forecast" // for test key
            //"options.method", "informers" // for other key
        );
        _client.configure(config);

        _client.open(null, (err) => {
            done(err);
        });
    });

    teardown((done) => {
        _client.close(null, done);
    });

    test('Test autoupdate weather cache', (done) => {

        async.series([
            (callback)=>{
                setTimeout(() => {
                    callback();
                }, 2000);
            },
            (callback)=>{
                _client.getWeatherFromCache("123", (err, cache)=>{
                    assert.isNull(err);
                    assert.isNotNull(cache);
                    assert.isObject(cache);
                    assert.isNotNull(cache.last_update);
                    assert.isObject(cache.weather);
                    callback();
                });
            }
        ], (err)=>{
            done();
        })
    });

});