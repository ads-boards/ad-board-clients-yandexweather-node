/** @module build */
import { Factory } from 'pip-services3-components-node';
import { Descriptor } from 'pip-services3-commons-node';

import { YandexWeatherClientV1 } from '../clients/YandexWeatherClientV1';

/**
 * Creates YandexWeather components by their descriptors.
 * 
 * @see [[YandexWeatherLogger]]
 */
export class DefaultYandexWeatherFactory extends Factory {
	public static readonly Descriptor = new Descriptor("ad-board-yandexweather", "factory", "datadog", "default", "1.0");
	public static readonly YandexWeatherClientDescriptor = new Descriptor("ad-board-yandexweather", "client", "direct", "*", "1.0");

	/**
	 * Create a new instance of the factory.
	 */
	public constructor() {
        super();
		this.registerAsType(DefaultYandexWeatherFactory.YandexWeatherClientDescriptor, YandexWeatherClientV1);
	}
}