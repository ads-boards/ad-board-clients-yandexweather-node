/** @module clients */
const _ = require('lodash');

import { ConfigParams, FixedRateTimer, IdGenerator, NotFoundException } from 'pip-services3-commons-node';
import { ConfigException } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { Parameters } from "pip-services3-commons-node";
import { CredentialResolver } from 'pip-services3-components-node';
import { RestClient, } from 'pip-services3-rpc-node';
import { WeatherCacheV1 } from './WeatherCacheV1';

export class YandexWeatherClientV1 extends RestClient {

    private _lat: string = '55.75396'; // Moscow
    private _lon: string  = '37.620393';
    private _lang: string = "ru_RU";
    private _method: string = 'informers';
    private _timer: FixedRateTimer = new FixedRateTimer();

    private _cache: WeatherCacheV1;
    private _correlationId: string = "ads.weather";
    private _interval: number = 30 * 60 * 1000; // 30 min;

    private _defaultConfig: ConfigParams = ConfigParams.fromTuples(
        "connection.protocol", "https",
        "connection.host", "api.weather.yandex.ru",
        "connection.port", 443,
        "credential.internal_network", "true"
    );
    private _credentialResolver: CredentialResolver = new CredentialResolver();

    public constructor(config?: ConfigParams) {
        super();

        if (config) this.configure(config);
        this._baseRoute = "v2";
    }

    public configure(config: ConfigParams): void {
        config = this._defaultConfig.override(config);
        super.configure(config);
        this._credentialResolver.configure(config);
        this._interval = config.getAsIntegerWithDefault("options.update_interval", this._interval);
        this._lon = config.getAsStringWithDefault("options.lon", this._lon);
        this._lat = config.getAsStringWithDefault("options.lat", this._lat);
        this._lang = config.getAsStringWithDefault("options.lang", this._lang);
        this._method = this._lang = config.getAsStringWithDefault("options.method", this._method);
    }

    public setReferences(refs: IReferences): void {
        super.setReferences(refs);
        this._credentialResolver.setReferences(refs);
    }

    public open(correlationId: string, callback: (err: any) => void): void {

        correlationId = correlationId || this._correlationId + " " + IdGenerator.nextLong();
        this._credentialResolver.lookup(correlationId, (err, credential) => {
            if (err) {
                callback(err);
                return;
            }

            if (credential == null || credential.getAccessKey() == null) {
                err = new ConfigException(correlationId, "NO_ACCESS_KEY", "Missing access key in credentials");
                callback(err);
                return;
            }

            this._headers = this._headers || {};
            this._headers['x-yandex-api-key'] = credential.getAccessKey();

            // start autoupdate weather into cache
            if (this._interval > 0) {
                this._timer.setDelay(this._interval);
                this._timer.setInterval(this._interval);
                this._timer.setTask({
                    notify: (correlationId: string, args: Parameters) => {
                        this._updateCache(correlationId);
                    }
                });
                this._logger.info(correlationId, "Update weather cache processing is enable");
                this._timer.start();
            }
            super.open(correlationId, callback);
        });
    }

    public close(correlationId: string, callback: (err: any) => any) {
        correlationId = correlationId || this._correlationId + " " + IdGenerator.nextLong();
        this._timer.stop();
        this._logger.info(correlationId, "Update weather cache processing i disable");
        super.close(correlationId, callback);
        
    }

    public getWeather(correlationId: string, callback: (err: any, weather: any) => void): void {
        let params = {
            lat: this._lat,
            lon: this._lon,
            lang: this._lang
        };

        let timing = this.instrument(correlationId, "ads.weather.get_weather");
        this.call("get", this._method, null, params, null, (err, result) => {
            timing.endTiming();
            this.instrumentError(correlationId, "ads.weather.get_weather", err, result, callback);
        });
    }

    public getWeatherFromCache(correlationId: string, callback: (err: any, weather: WeatherCacheV1) => void): void {

        let timing = this.instrument(correlationId, "ads.weather.get_weather_from_cache");
        if (this._cache != null && this._cache.weather != null) {
            timing.endTiming();
            callback(null, this._cache);
        } else {
            this.getWeather(correlationId, (err, weather) => {
                if (err) {
                    timing.endTiming();
                    this.instrumentError(correlationId, "ads.weather.get_weather_from_cache", err, null, callback);
                    return;
                }
                if (weather == null) {
                    err = new NotFoundException(correlationId, "NOT_FOUND", "Cache is empty and can't get weather from service");
                    timing.endTiming();
                    this.instrumentError(correlationId, "ads.weather.get_weather_from_cache", err, null, callback);
                    return;
                }
                this._cache = this._cache || new WeatherCacheV1();
                this._cache.last_update = new Date();
                this._cache.weather = weather;
                callback(null, this._cache);
            });
        }
    }

    private _updateCache(correlationId: string): void {

        let timing = this.instrument(correlationId, "ads.weather.update_weather_cache");
        this.getWeather(correlationId, (err, weather) => {
            if (err) {
                timing.endTiming();
                this.instrumentError(correlationId, "ads.weather.update_weather_cache", err, weather, null);
                return;
            }
            if (weather == null) {
                err = new NotFoundException(correlationId, "NOT_FOUND", "The weather not available in service");
                timing.endTiming();
                this.instrumentError(correlationId, "ads.weather.update_weather_cache", err, weather, null);
                return;
            }
            this._cache = this._cache || new WeatherCacheV1();
            this._cache.last_update = new Date();
            this._cache.weather = weather;
        });
    }
}